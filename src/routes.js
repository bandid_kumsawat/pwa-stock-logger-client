import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/layouts/DashboardLayout';
import MainLayout from 'src/layouts/MainLayout';
import StockView from 'src/views/Stock/ListStock';
import ManageStock from 'src/views/Stock/ManageStock'
import ResultSearch from 'src/views/Stock/ManageStock/ResultSearch'
import Dashboard from 'src/views/Stock/Dashboard'

const routes = [
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      { path: 'Dashboard', element: <Dashboard /> },
      { path: 'Stock', element: <StockView /> },
      { path: "Manage", element: <ManageStock /> },
      { path: "Logger/:DEVICE", element: <ResultSearch /> },
      { path: '*', element: <StockView /> }
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: '/', element: <Navigate to="/app/Dashboard" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
