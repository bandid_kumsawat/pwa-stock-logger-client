import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import App from './App';
import { store } from './AppStore/store'

ReactDOM.render((
  <HashRouter>
    <App store={store} />
  </HashRouter>
), document.getElementById('root'));

serviceWorker.unregister();
