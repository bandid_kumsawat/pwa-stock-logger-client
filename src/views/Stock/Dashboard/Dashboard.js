import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import server from 'src/apis'
import { STOREPROVICE } from 'src/AppStore/actions'
import {
  Container,
  Grid
} from '@material-ui/core'

const useCardStyle = makeStyles({
  root:{
    display: "flex",
  },
  card: {
    minWidth: '100%',
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 18,
    minHeight: 40
  },
  pos: {
    marginBottom: 12,
  },
  content: {
    minWidth: "100%",
  },
  value: {
    fontSize: 30,
    fontWeight: 700,
    minHeight: 40,
    color: "#2c94ff"
  }
});

export default function Dashboard(props) {
  const [store] = React.useState(props.store)
  const [summary, setsummary] = React.useState([])
  const classes = useCardStyle();
  /**
   * summary format
   *    
   *  [
    Instock: Number,        เหลืออยู่ในสต๊อก
    Takeout: Number,        กำลังนำออกไปติดตั้ง
    Installed: Number,      ติดตั้ง
    Maintenance: Number,    ส่งซ่อม
    Stock: Number,          logger ทั้งหมด
    ProviceInstallLoggerRate: [
      {
        area: String,       เขต
        value: Number       จำนวน
      },{
        area: String,       เขต
        value: Number       จำนวน
      },{
        area: String,       เขต
        value: Number       จำนวน
      },
      ..... 10 สาขา
    ],
    ProviceInstallLoggerChart: [
      {
        area: String,       เขต
        value: Number,      จำนวน
      }
    ]
   ]
   */

  React.useEffect(() => {
    const handleData = async () => {
      const resProvice = await server().get('/logger/stock/provice')
      store.dispatch(STOREPROVICE(resProvice.data))
      var Instock=0,Takeout=0,Maintenance=0,Installed=0
      var Stock = store.getState().Stock
      var Provice = store.getState().Provice
      var ListAreaInstall = []
      var ListLoggerRate = []
      // gen format
      Provice.forEach((itemProvice, indexProvice) => {
        if (itemProvice.reg_desc !== 'กปภ.สำนักงานใหญ่'){
          ListAreaInstall.push({
            area: itemProvice.reg_desc,
            value: 0
          })
        }
      })

      Stock.forEach((itemStock, indexStock) => {
        ListAreaInstall.forEach((itemList, indexList) => {
          if (itemStock.area !== 'กปภ.สำนักงานใหญ่'){
            if (itemStock.area === itemList.area && itemStock.status === 'Installed'){
              ListAreaInstall[indexList].value++
            }
          }
        })
      })

      ListAreaInstall = ListAreaInstall.sort((a,b) => (a.value < b.value) ? 1 : ((b.value < a.value) ? -1 : 0))

      if (ListAreaInstall.length > 3){
        for (var i = 0;i < 3;i++){
          if (ListAreaInstall[i].value > 0){
            ListLoggerRate.push(ListAreaInstall[i])
          }
        }
      }else{
        for (var i = 0;i < ListAreaInstall.length;i++){
          if (ListAreaInstall[i].value > 0){
            ListLoggerRate.push(ListAreaInstall[i])
          }
        }
      }

      Stock.forEach((item, index) => {
        switch (item.status_) {
          case 'Instock':
            Instock++
            break;
          case 'Takeout':
            Takeout++
            break;
          case 'Installed':
            Installed++
            break;
          case 'Maintenance':
            Maintenance++
          break;
        }
      })
      setsummary([{
        Instock: Instock,
        Takeout: Takeout,
        Installed: Installed,
        Maintenance: Maintenance,
        Stock: Stock.length,
        ProviceInstallLoggerRate: ListLoggerRate,
        ProviceInstallLoggerChart: ListAreaInstall
      }])
      console.log([{
        Instock: Instock,
        Takeout: Takeout,
        Installed: Installed,
        Maintenance: Maintenance,
        Stock: Stock.length,
        ProviceInstallLoggerRate: ListLoggerRate,
        ProviceInstallLoggerChart: ListAreaInstall
      }])
    }
    handleData()
  }, [])

  const ShowStatus = (props) => {
    const classes = useCardStyle();
    return (
      <Card className={classes.root} variant="outlined">
        <CardContent className={classes.content}>
          <div className={classes.title}><center>{props.title}</center></div>
          <div className={classes.value}><center>{props.value}</center></div>
        </CardContent>
      </Card>
    )
  }

  return (
    <div className={classes.root}>
        <Container maxWidth={false}>

          <Grid
            container
            spacing={3}
          >
            {
              ((summary[0] !== undefined) ? 
              <>
                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"อยู่ในสต๊อก"} value={summary[0].Instock} />
                </Grid>

                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"ออกจากสต๊อก"} value={summary[0].Takeout} />
                </Grid>

                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"ติดตั้งแล้ว"} value={summary[0].Installed} />
                </Grid>

                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"ซ่อมบำรุง"} value={summary[0].Maintenance} />
                </Grid>

                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"ซ่อมบำรุง"} value={summary[0].Maintenance} />
                </Grid>


                <Grid
                  item
                  lg={2}
                  md={2}
                  xl={2}
                  xs={12}
                >
                  <ShowStatus title={"ซ่อมบำรุง"} value={summary[0].Maintenance} />
                </Grid>

              </>
            : "")
            }
          </Grid>
        
          

        </Container>
      
    </div>
  );
}
