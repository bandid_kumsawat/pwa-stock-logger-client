
import React from 'react';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container,
  Grid,
  FormControl,
  Button,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core';

import { RESULTSEARCHSTOCK } from '../../../AppStore/actions'
import SearchByAraeAndBranch from './SearchByAraeAndBranch'

const useStyles = makeStyles(() => ({
  root: {},
  formControl: {
    margin: '10px',
    minWidth: '100%',
  },
  btnsearch: {
    marginTop: "10px",
    minWidth: '100%'
  }
}));

const Form = ({ className, store,  ...rest}) => {
  const classes = useStyles();

  const [brancharr, setbrancharr] = React.useState([])
  const [data, setdata] = React.useState([])
  const [state, setState] = React.useState({
    area: 'REG0',
    branch: '',
    store: store
  });

  const handleAreaChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
    state.store.getState().Provice.forEach((item, index) => {
      if (item.reg === event.target.value){
        setbrancharr(item.ww_all)  
      }
    })
  }

  const handleBranchChange = (event) => {
    const name = event.target.name;
    setState({
      ...state,
      [name]: event.target.value,
    });
  }

  const handleSearch = (e) => {
    var temp = state.store.getState().Stock
    var provice = state.store.getState().Provice
    var area = '', branch = ''
    provice.forEach((item ,index) => {
      if (item.reg === state.area){
        area = item.reg_desc
        item.ww_all.forEach((itemb, indexb) => {  
          if (state.branch === itemb.ww){
            branch = itemb.ww_desc
          }
        })
      }
    })
    var resultSearch = []
    temp.forEach((item ,index) => {
      if (item.area === area && item.branch === branch){
        console.log(item)
        resultSearch.push(item)
      }
    })
    state.store.dispatch(RESULTSEARCHSTOCK(resultSearch))
    setdata(resultSearch)
  }

  return (
    <div>
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            lg={3}
            md={12}
            xl={3}
            xs={12}
          >
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel id="demo-simple-select-filled-label">กรุณาเลือกเขต</InputLabel>
              <Select
                labelId="demo-simple-select-filled-label"
                id="demo-simple-select-filled"
                name="area"
                value={state.area}
                onChange={handleAreaChange}
              >
                {
                  ((state.store.getState().Provice.length === 0) ? '' : state.store.getState().Provice.map((item, index) => { 
                    return <MenuItem key={item.reg} value={item.reg}>
                      <em>{item.reg_desc}</em>
                    </MenuItem>
                  }))
                }
              </Select>
            </FormControl>
            {
            }
          </Grid>
          <Grid
            item
            lg={4}
            md={12}
            xl={4}
            xs={12}
            hidden={state.area === 'REG0' || state.area === '' || brancharr.length === 0 }
          >
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel id="demo-simple-select-filled-label">กรุณาเลือกสาขา</InputLabel>
              <Select
                labelId="demo-simple-select-filled-label"
                id="demo-simple-select-filled"
                name="branch"
                value={state.branch}
                onChange={handleBranchChange}
              >
                {
                  ((brancharr.length === 0) ? '' : brancharr.map((item, index) => { 
                    return <MenuItem key={item.ww} value={item.ww}>
                      <em>{item.ww_desc}</em>
                    </MenuItem>
                  }))
                }
              </Select>
            </FormControl>
          </Grid>
          <Grid
            item
            lg={4}
            md={12}
            xl={4}
            xs={12}
          >
            <FormControl variant="outlined" className={classes.formControl}>
              <Button 
                variant="contained" 
                color="primary" 
                className={classes.btnsearch} 
                onClick={handleSearch}
              >
                ค้นหาอุปกรณ์
              </Button>
            </FormControl>
          </Grid>
        </Grid>
      </Container>
      <SearchByAraeAndBranch data={data} store={state.store}/>
    </div>
  );
};

Form.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default Form;