import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import TablePagination from '@material-ui/core/TablePagination';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const getstatuslog = (status) => {
  if (status === 'Instock'){
    return <span style={{color: "#777676", fontWeight: "700"}}>อยู่ในสต๊อก</span>
  }else if (status === 'Takeout'){
    return <span style={{color: "#00468e", fontWeight: "700"}}>กำลังนำไปติดตั้ง</span>
  }else if (status === 'Installed'){
    return <span style={{color: "#0cb112", fontWeight: "700"}}>ติดตั้งแล้ว</span>
  }else if (status === 'Maintenance'){
    return <span style={{color: "#e91e63", fontWeight: "700"}}>กำลังซ่อมบำรุง</span>
  } 
}

function Row(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();
  console.log(props)
  return (
    <React.Fragment >
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            ประวัติ{open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.serial}</TableCell>
        <TableCell align="center">{row.area}</TableCell>
        <TableCell align="center">{row.branch}</TableCell>
        <TableCell align="center">{row.status}</TableCell>
        <TableCell align="center">{((row.time_instock === '' ? "-": row.time_instock))}</TableCell>
        <TableCell align="center">{((row.time_takeout === '') ? "-" : row.time_takeout)}</TableCell>
        <TableCell align="center">{((row.time_installed === '') ? "-" : row.time_installed)}</TableCell>
        <TableCell align="center">{((row.time_maintenance === '') ? "-" : row.time_maintenance)}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>

          {/* section extent when user click */}
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                ประวัติ
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead style = {{backgroundColor: "#00468e1a"}}>
                  <TableRow>
                    <TableCell>Serial No.</TableCell>
                    <TableCell>สถานะ</TableCell>
                    <TableCell align="center">เขต</TableCell>
                    <TableCell align="center">สาขา</TableCell>
                    <TableCell align="center">เวลาและวันที่</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody style = {{backgroundColor: "#00468e1a"}}>
                  {row.log.map((historyRow, index) => (
                    <TableRow key={index}>
                      <TableCell component="th" scope="row">
                        {historyRow.serial}
                      </TableCell>
                      <TableCell>
                        {
                          // historyRow.status  
                          getstatuslog(historyRow.status)
                        }
                      </TableCell>
                      <TableCell align="center">{historyRow.area}</TableCell>
                      <TableCell align="center">{historyRow.branch}</TableCell>
                      <TableCell align="center">
                        {
                          historyRow.datetime
                        }
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>

        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    serial: PropTypes.string.isRequired,
    area: PropTypes.string.isRequired,
    branch: PropTypes.string.isRequired,

    status: PropTypes.string.isRequired,
    time_instock: PropTypes.string.isRequired,
    time_takeout: PropTypes.string.isRequired,
    time_installed: PropTypes.string.isRequired,
    time_maintenance: PropTypes.string.isRequired,
    log: PropTypes.arrayOf(
      PropTypes.shape({
        serial: PropTypes.string.isRequired,
        status: PropTypes.string.isRequired,
        area: PropTypes.string.isRequired,
        branch: PropTypes.string.isRequired,
        datetime: PropTypes.string.isRequired,
      }),
    ).isRequired,
  }).isRequired,
};

export default function CollapsibleTable(props) {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [store] = React.useState(props.store)

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  React.useEffect(() => {
    async function callapi() {

    }
    callapi()
  })

  return (
    <TableContainer>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell align="center">Serial No.</TableCell>
            <TableCell align="center">เขต</TableCell>
            <TableCell align="center">สาขา</TableCell>
            <TableCell align="center">สถานะ</TableCell>
            <TableCell align="center">วันที่เข้าสต๊อก</TableCell>
            <TableCell align="center">วันที่ออกจากสต๊อก</TableCell>
            <TableCell align="center">วันที่ติดตั้งเสร็จ</TableCell>
            <TableCell align="center">ซ่อมบำรุงล่าสุด</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            store.getState().Stock.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              var status = row.status
              if (status === 'Instock'){
                row.status = <span style={{color: "#777676", fontWeight: "700"}}>อยู่ในสต๊อก</span>
              }else if (status === 'Takeout'){
                row.status = <span style={{color: "#00468e", fontWeight: "700"}}>กำลังนำไปติดตั้ง</span>
              }else if (status === 'Installed'){
                row.status = <span style={{color: "#0cb112", fontWeight: "700"}}>ติดตั้งแล้ว</span>
              }else if (status === 'Maintenance'){
                row.status = <span style={{color: "#e91e63", fontWeight: "700"}}>กำลังซ่อมบำรุง</span>
              }
              // var month_th = ["มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"]
              // var month_en = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
              
              return (
                <Row key={index} row={row} />
              );
            })
          }
        </TableBody>
      </Table>
      <TablePagination
        rowsPerPageOptions={[5,10, 25, 100]}
        component="div"
        count={store.getState().Stock.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        SelectProps={{
          inputProps: { 'aria-label': 'แถวต่อหน้า' },
          native: true,
        }}
      />
    </TableContainer>
  );
}
