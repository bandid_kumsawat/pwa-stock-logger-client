
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import SearchAllStock from './SearchStock'
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  makeStyles
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {}
}));

const ViewStock = ({ className, store,  ...rest}) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader
        title="รายการล๊อกเกอร์"
      />
      <Divider />
      <CardContent>
        <SearchAllStock store={store}/>
      </CardContent>
      <Divider />
      <Box
        display="flex"
        justifyContent="flex-end"
        p={2}
      >
      </Box>
    </Card>
  );
};

ViewStock.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default ViewStock;