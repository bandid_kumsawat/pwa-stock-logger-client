
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container,
  Grid,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Box,
  Button,
  LinearProgress,
  CircularProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from '@material-ui/core';
import {
  Autocomplete
} from '@material-ui/lab';
import Page from 'src/components/Page';
import {
  useParams
} from "react-router-dom";
// import { store } from './../../../AppStore/store'
// import { STORESTOCK, STOREPROVICE } from './../../../AppStore/actions'
import server from './../../../apis'
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  cardroot:{},
  title: {
    // fontSize: "24px",
    marginBottom: "6px",
    fontWeight: "600"
  },
  value:{
    // fontSize: "18px",
    fontWeight: "100"
  },
  CardHeaderStyle: {
    fontSize: "30px !important" 
  }
}));

const InsertDevice = ({ className, data, ...rest}) => {
  const classes = useStyles();
  var { DEVICE } = useParams();
  const [device] = React.useState(DEVICE)
  const [check_insert, setcheck_insert] = React.useState(false)

  const handleInsertLogger = async () => {
    setcheck_insert(true)
    const res = await server().put("/logger/add", {
      "serial": device,
      "area": "กปภ.สำนักงานใหญ่",
      "branch": "",
      "status": "Instock",
      "time_instock": moment(new Date()).format("L h:mm:ss a"),
      "time_takeout": "",
      "time_installed": ""
    })
    if (res.data.status && typeof res.data.status !== undefined){
      window.location.replace('/#/app/Logger/' + device)
    }
  }

  return (
      <Grid
        container
        spacing={1}
      >
        <Grid
          item
          lg={2}
          md={2}
          xl={2}
          xs={6}
          className={classes.title}
        > รหัส : </Grid>
        <Grid
          item
          lg={2}
          md={2}
          xl={2}
          xs={6}
          className={classes.value}
        >
          {device}
          {
            ((check_insert) ? <CircularProgress /> : "")
          }
        </Grid> 
        <Grid
          item
          lg={2}
          md={2}
          xl={2}
          xs={6}
          className={classes.value}
        >
          <Button 
            variant="contained" 
            color="primary" 
            className={classes.btnsearch} 
            onClick={handleInsertLogger}
          >
            {'เพิ่มล๊อกเกอร์ในสต๊อก'}
          </Button>
        </Grid> 
      </Grid>
    )
}


const EditDevice = ({ className, ...rest}) => {
  const classes = useStyles();

  var { DEVICE } = useParams();
  // const [device] = React.useState(DEVICE)
  const [ListParam, setListParam] = React.useState([undefined])
  const [StatusLgg, setStatusLgg] = React.useState('')
  const [open, setOpen] = React.useState(false);
  const [provice, setprovice] = React.useState([])
  const [area, setarea] = React.useState([])
  const [branch, setbranch] = React.useState([])
  const [selectarea, setselectarea] = React.useState('')
  const [selectranch, setselectranch] = React.useState('')
  const [selectstatus, setselectstatus] = React.useState('')
  const [logger, setlogger] = React.useState([])


  const handleUpdateStatus = async (e) => {
    if ((selectarea !== '' && selectranch !== '') || selectarea === 'กปภ.สำนักงานใหญ่'){
      setOpen(false);
      var req = {}
      if (selectstatus === 'Instock'){
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_instock": moment(new Date()).format("L h:mm:ss a"),
        }
      }else if (selectstatus === 'Takeout') {
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_takeout": moment(new Date()).format("L h:mm:ss a"),
        }
      }else if (selectstatus === 'Installed') {
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_installed": moment(new Date()).format("L h:mm:ss a"),
        }
      }else if (selectstatus === 'Maintenance'){
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_maintenance": moment(new Date()).format("L h:mm:ss a"),
        }
      }
      console.log(req)
      const res = await server().post('/logger/update/id', req)
      console.log(res.data)
      window.location.replace('/#/app/Logger/' + DEVICE)
    }
  }

  const handleOpenDialog = (status) => {
    setselectarea("")
    setselectranch('')
    setbranch([])
    setselectstatus(status)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false);
  };

  const manageStatus = (tag, status, value) => {
    // time_instock time_takeout time_installed time_maintenance
    // Installed Instock Takeout Maintenance
    if (tag === 'time_instock') {
      return ((value === "-") ? "-": moment(value, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss') )
    }else if (tag === 'time_takeout'){
      if (status === 'Instock'){
        return <>
          <Button 
            variant="contained" 
            color="primary" 
            className={classes.btnsearch} 
            onClick={() => {
              handleOpenDialog("Takeout")
            }}
          >
            {'นำล๊อกเกอร์ออกจากสต๊อก'}
          </Button>
        </>
      }else{
        return ((value === "-") ? "-": moment(value, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss') )
      }
    }else if (tag === 'time_installed'){
      if (status === 'Takeout' || status === 'Maintenance'){
        return <>
          <Button 
            variant="contained" 
            color="primary" 
            className={classes.btnsearch} 
            onClick={() => {
              handleOpenDialog("Installed")
            }}
          >
            {'ติดตั้งล๊อกเกอร์'}
          </Button>
        </>
      }else {
        return ((value === "-") ? "-": moment(value, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss') )
      }
    }else if (tag === 'time_maintenance'){
      if (status === 'Installed'){
        return <>
          <Button 
            variant="contained" 
            color="primary" 
            className={classes.btnsearch} 
            onClick={() => {
              handleOpenDialog("Maintenance")
            }}
          >
            {'ซ่อมล๊อกเกอร์'}
          </Button>
        </>
      }else {
        return ((value === "-") ? "-": moment(value, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss') )
      }
    }else if (tag === 'status'){
      // console.log(moment('11/13/2020 1:26:20 PM', "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss'))
      // Installed Instock Takeout Maintenance
      if (value === 'Instock'){
        return 'อยู่คงคลัง'
      }else if (value === 'Takeout'){
        return 'กำลังนำไปติดตั้ง'
      }else if (value === 'Installed'){
        return 'ติดตั้งแล้ว'
      }else if (value === 'Maintenance'){
        return 'ซ่อมบำรุง'
      }
    } else {
      return value
    }
  }

  React.useEffect(() => {
    const findDevice = async () => {
      const res = await server().post('/logger/find/id', {
        "serial": DEVICE
      })
      setlogger(res.data)
      const resProvice = await server().get('/logger/stock/provice')
      setprovice(resProvice.data)
      setarea(resProvice.data.map((item) => {
        return {
          area: item.reg_desc,
          branch: item.ww_all
        }
      }))
      if (res.data.length !== 0){
        var data = res.data[0]
        setStatusLgg(data.status)
        var temp = [
          {
            title: "รหัส",
            value: data.serial,
            tag: "serial"
          },{
            title: "เขต",
            value: data.area,
            tag: "area"
          },{
            title: "สาขา",
            value: data.branch,
            tag: "branch"
          },{
            title: "สถานะ",
            value: data.status,
            tag: "status"
          },{
            title: "วันเข้าสต๊อก",
            value: ((data.time_instock === "") ? "-" : data.time_instock ),
            tag: "time_instock"
          },{
            title: "วันออกสต๊อก",
            value: ((data.time_takeout === "") ? "-" : data.time_takeout ),
            tag: "time_takeout"
          },{
            title: "วันติดตั้ง",
            value: ((data.time_installed === "") ? "-" : data.time_installed ),
            tag: "time_installed"
          },{
            title: "วันซ่อมบำรุง",
            value: ((data.time_maintenance === "") ? "-" : data.time_maintenance ),
            tag: "time_maintenance"
          },
        ]
        setListParam(temp) 
      }else {
        setListParam([]) 
      }
    }
    findDevice()
  }, [DEVICE])

  return (
    <Page
      className={classes.root}
      title="จัดการข้อมูล"
      {...rest}
    >
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        fullWidth={true}
        maxWidth={'sm'}
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title">
          {
            ((selectstatus === "Takeout") ? "เลือกเขตและสาขาที่ต้องการนำล๊อกเกอร์ไปติดตั้ง": ((selectstatus === 'Installed') ? "เลือกเขตและสาขาที่ต้องการนำล๊อกเกอร์ติดตั้งเสร็จแล้ว" : ((selectstatus === "Maintenance") ? "เลือกเขตและสาขาที่ต้องการนำล๊อกเกอร์ไปซ่อมบำรุง" : "")))
          }
        </DialogTitle>
        <DialogContent>
        <Autocomplete
            id="combo-box-area"
            options={area}
            getOptionLabel={(option) => option.area}
            style={{ width: '100%' }}
            onChange={(event, data) => {
              if (data !== null){
                setselectarea(data.area)
                setbranch(data.branch.map((item) => {
                  return {
                    branch: item.ww_desc
                  }
                }))
              }
            }}
            renderInput={(params) => <TextField {...params} label="เลือกเขต" variant="outlined" />}
          />
          <br></br>
          <Autocomplete
            hidden={branch.length === 0 || branch.length === 1}
            id="combo-box-branch"
            options={branch}
            clearOnEscape={true}
            getOptionLabel={(option) => option.branch}
            style={{ width: '100%' }}
            onChange={(e, d) => {
              if (d !== null){
                setselectranch(d.branch)
              }
            }}
            renderInput={(params) => <TextField {...params} label="เลือกสาขา" variant="outlined" />}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleUpdateStatus} color="primary">
            ตกลง
          </Button>
          <Button onClick={handleClose} color="primary">
            ยกเลิก
          </Button>
        </DialogActions>
      </Dialog>
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        ><Grid
            item
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            <Card
              className={clsx(classes.cardroot, className)}
            >
              <CardHeader
                // title={((ListParam.length === 0) ? "จัดการข้อมูลล๊อกเกอร์": "แก้ไขข้อมูลล๊อกเกอร์")}
                className={classes.CardHeaderStyle}
                title={((ListParam[0] === undefined) ? ((ListParam.length === 0) ? "เพิ่มล๊อกเกอร์": "กำลังค้นหาล๊อกเกอร์ . . . ") : "ผลลัพท์การค้นหา")}
              />
              <Divider />
              <CardContent>
                
                <Container maxWidth={false}>
                  {
                    ((ListParam[0] === undefined) ? ((ListParam.length === 0) ? <InsertDevice /> : <LinearProgress />) : ListParam.map((item, index) => {
                      return (
                        <Grid
                          key={item.tag}
                          container
                          spacing={1}
                        >
                          <Grid
                            item
                            lg={2}
                            md={2}
                            xl={2}
                            xs={6}
                            className={classes.title}
                          > {item.title} : </Grid>
                          <Grid
                            item
                            lg={2}
                            md={2}
                            xl={2}
                            xs={6}
                            className={classes.value}
                          > {

                            // time_instock time_takeout time_installed time_maintenance
                            manageStatus(item.tag, StatusLgg, item.value)

                          } </Grid>
                        </Grid>
                      )
                    }))
                  }
                </Container>

              </CardContent>
              <Divider />
              <Box
                display="flex"
                justifyContent="flex-end"
                p={2}
              >
                <Button 
                  variant="contained" 
                  color="primary" 
                  className={classes.btnsearch} 
                  onClick={
                    () => {
                      window.location.replace('/#/app/Manage')
                    }
                  }
                >
                  {'ออก'}
                </Button>
              </Box>
            </Card>
          
          </Grid>
        </Grid>
      </Container>
    </Page>
  )
};

EditDevice.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default EditDevice;