
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  makeStyles,
  Container,
  Grid,
  Button,
  TextField,
  Snackbar,
  LinearProgress 
} from '@material-ui/core';

import {
  Autocomplete
} from '@material-ui/lab';
import server from './../../../apis'
import parse from 'csv-parse/lib/sync'
import MuiAlert from '@material-ui/lab/Alert';
import { STORESTOCK } from './../../../AppStore/actions'
import QrScan from './QrScan'
import moment from 'moment'

const object_month = {
  January: "มกราคม",
  February: "กุมภาพันธ์",
  March: "มีนาคม",
  April: "เมษายน",
  May: "พฤษภาคม",
  June: "มิถุนายน",
  July: "กรกฎาคม",
  August: "สิงหาคม",
  September: "กันยายน",
  October: "ตุลาคม",
  November: "พฤศจิกายน",
  December: "ธันวาคม", 
}

const useStyles = makeStyles(() => ({
  root: {},
  btnsearch: {
    minWidth: '100%',
    marginTop: '10px',
  },
  FIELDSET: {
    margin: '8px',
    border: '1px solid silver',
    padding: '8px',
    borderRadius: '4px',
  },
  LEGEND: {
    padding: "2px"
  },
  containerimportexport: {
    padding: "20px"
  },
  formnest: {
    minWidth: "100%"
  },
  statusuploadfilecsv: {
    marginTop: '5px'
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const FormSearchDevice = ({ className, store,  ...rest}) => {
  const classes = useStyles();
  const [listdevice, setlistdevice] = React.useState([])
  const [openQR, setopenQR] = React.useState([])
  const [state, setState] = React.useState({
    openSnack: false,
    vertical: 'top',
    horizontal: 'center',
    uploadFileStatus: false,
  });

  const [statusProgress, setstatusProgress] = React.useState(false)
  const [TypeInAutoInput, setTypeInAutoInput] = React.useState('')

  const { vertical, horizontal, openSnack, uploadFileStatus } = state;

  const inputOpenFileRef = React.createRef()

  const handleUploadClick = event => {
    console.log("upload file !")
    file = event.target.files[0];
    reader.readAsText(file, "UTF-8");
  };

  const reader = new FileReader()
  var file = null;

  reader.onloadend = async function(e) {
    console.log(e.target.result)
    var csv = e.target.result
    const records = {}
    records.data = parse(csv, {columns: true});
    setstatusProgress(true)
    const res = await server().post('/logger/upload/csv', records)
    if (res.data.status && typeof res.data.status !== undefined){
      setstatusProgress(false)
      file = null
      setState({ openSnack: true, ...{ vertical: 'bottom', horizontal: 'left' }, uploadFileStatus: true });
      window.location.replace('/#/app/Manage')
    }else{
      setState({ openSnack: true, ...{ vertical: 'bottom', horizontal: 'left' }, uploadFileStatus: false });
    }
  }

  reader.onerror = (evt) => {
    console.log("error reading file")
  }

  const handleClose = () => {
    setState({ ...state, openSnack: false });
  };

  React.useEffect(() => {
    async function callapi() {
      const ResultStockForListDevice = await server().get('/logger/find/all')
      setlistdevice(ResultStockForListDevice.data.map((item, index) => {  
        return item.serial
      }))
      // const res = await server().get('/logger/stock/history')
      // store.dispatch(STORESTOCK(res.data))
      // setdata(store.getState().Stock)
      // const resProvice = await server().get('/logger/stock/provice')
      // store.dispatch(STOREPROVICE(resProvice.data))
    }
    async function callStock(){
      const res = await server().get('/logger/stock/history')
      if (res.status === 200){
        res.data = res.data.map((row, index) => {
          var cvalue = row.time_instock
          var month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_instock =  ((row.time_instock === "") ? "" : moment(row.time_instock, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_takeout
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_takeout =  ((row.time_takeout === "") ? "" : moment(row.time_takeout, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_installed
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_installed =  ((row.time_installed === "") ? "" : moment(row.time_installed, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_maintenance
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_maintenance =  ((row.time_maintenance === "") ? "" : moment(row.time_maintenance, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          return row
        })
      }
      store.dispatch(STORESTOCK(res.data))
    }
    callapi()
    callStock()
  }, [store])

  const handleExportCSV = async () => {
    const ConvertToCSV = (objArray) => {
        var array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
        var str = 'serial,area,branch,status,time_instock,time_takeout,time_installed,time_maintenance\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line !== '') line += ','
                delete array[i].log
                line += array[i][index];
            }

            str += line + '\r\n';
        }

        return str;
    }
    const res = await server().get('/logger/stock/history')
    console.log(res)
    var data = ConvertToCSV(res.data)
    const url = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', 'PWA_Stock_Logger_Export.csv'); //or any other extension
    document.body.appendChild(link);
    link.click();
  }

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader
        title="ค้นหาล๊อกเกอร์"
      />
      <Divider />
      <CardContent>
        
        <Container maxWidth={false}>
          {/* >>>>>> show status <<<<<<< */}
          <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            open={openSnack}
            onClose={handleClose}
            message="hello"
            key={vertical + horizontal}
          >
            {
              // uploadFileStatus
              ((uploadFileStatus) ? <Alert onClose={handleClose} severity="success">
              อัพโหลดไฟล์เสร็จสิ้น
            </Alert> : <Alert onClose={handleClose} severity="error">
              อัพโหลดไฟล์ไม่สำเร็จ
            </Alert>)
            }
          </Snackbar>
          
        {/* >>>>> Search Device by Qr code <<<<<<<< */}
        <Grid
            container
            spacing={1}
          >
            <Grid
              item
              xl={12}
              lg={12}
              md={12}
              xs={12}
            >
            <fieldset className={classes.FIELDSET}>
              <legend className={classes.LEGEND}>ค้นหาล๊อกเกอร์จาก Qr Code</legend>
              <div>
                <Container maxWidth={false} className={classes.containerimportexport}>
                  <Grid
                    container
                    spacing={1}
                  >
                    <Grid
                      item
                      lg={4}
                      md={4}
                      xl={4}
                      xs={12}
                    >
                      <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.btnsearch} 
                        onClick={
                          () => {
                            setopenQR(!openQR)
                          }
                        }
                      >
                        {((!openQR) ? "เปิด": "ปิด") + 'สแกน Qr Code'}
                      </Button>
                      <br></br>
                      <QrScan hidden={!openQR}/>
                    </Grid>
                    <Grid 
                      item
                      lg={1}
                      md={1}
                      xl={1}
                      xs={12}
                    >
                    </Grid>
                  </Grid>
                </Container>
              </div>
            </fieldset>
            </Grid>
          </Grid>

          {/* >>>>> Search Device by "Auto complete" <<<<<<<< */}
          <Grid
            container
            spacing={1}
          >
            <Grid
              item
              xl={12}
              lg={12}
              md={12}
              xs={12}
            >
            <fieldset className={classes.FIELDSET}>
              <legend className={classes.LEGEND}>ค้นหาล๊อกเกอร์จากรหัส</legend>
              <div>
                <Container maxWidth={false} className={classes.containerimportexport}>
                  <Grid
                    container
                    spacing={1}
                  >
                    <Grid
                      item
                      lg={4}
                      md={4}
                      xl={4}
                      xs={12}
                    >
                      <Autocomplete
                        freeSolo
                        className={classes.formnest}
                        id="combo-box-demo"
                        options={listdevice}
                        getOptionLabel={(item) => item}
                        style={{ width: 300 }}
                        onInputChange={(event, newInputValue) => {
                          setTypeInAutoInput(newInputValue);
                        }}
                        onChange={(event, value) => window.location.replace('/#/app/Logger/'+value)} 
                        renderInput={(params) => <TextField {...params} label="กรอกรหัสล๊อกเกอร์" variant="outlined" />}
                      />
                    </Grid>
                    <Grid 
                      item
                      lg={1}
                      md={1}
                      xl={1}
                      xs={12}
                    >
                    </Grid>
                    <Grid
                      item
                      lg={4}
                      md={4}
                      xl={4}
                      xs={12}
                    >
                      <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.btnsearch} 
                        onClick={(event, value) => window.location.replace('/#/app/Logger/'+TypeInAutoInput)} 
                        // onClick={handleSearch}
                      >
                        ค้นหา
                      </Button>
                    </Grid>
                  </Grid>
                </Container>
              </div>
            </fieldset>
            </Grid>
          </Grid>
          {/* >>>>>>>>>>>>>>>>>>> section import and export csv file <<<<<<<<<<<<<<<<<<<<<<<<<<<< */}
          <Grid
            container
            spacing={1}
          >
            <Grid
              item
              xl={12}
              lg={12}
              md={12}
              xs={12}
            >
            <fieldset className={classes.FIELDSET}>
              <legend className={classes.LEGEND}>จัดการข้อมูล</legend>
              <div>
                <Container maxWidth={false} className={classes.containerimportexport}>
                  <Grid
                    container
                    spacing={1}
                  >
                    <Grid
                      item
                      lg={4}
                      md={4}
                      xl={4}
                      xs={12}
                    >
                      {
                        // rerender <input /> file for read new file original name
                        ((!statusProgress) ? <input 
                        ref={inputOpenFileRef} 
                        accept=".csv"
                        multiple
                        type="file"
                        style={{ display: "none" }}
                        onChange={handleUploadClick}
                      />  : "")
                      }
                      <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.btnsearch} 
                        type="file"
                        onClick={() => {
                          inputOpenFileRef.current.click()
                        }}
                      >
                        Import .csv
                      </Button>
                    </Grid>
                    <Grid 
                      item
                      lg={1}
                      md={1}
                      xl={1}
                      xs={12}
                    >
                    </Grid>
                    <Grid
                      item
                      lg={4}
                      md={4}
                      xl={4}
                      xs={12}
                    >
                      <Button 
                        variant="contained" 
                        color="primary" 
                        className={classes.btnsearch} 
                        onClick={handleExportCSV}
                      >
                        Export .csv
                      </Button>
                    </Grid>
                  </Grid>
                  <Grid
                    item
                    lg={12}
                    md={12}
                    xl={12}
                    xs={12}
                  >
                    {
                      ((statusProgress) ? <div className={classes.statusuploadfilecsv}>กำลังอัพโหลดไฟล์กรุณารอซักครู่....</div> : "")
                    }
                    {
                      ((statusProgress) ? <LinearProgress  className={classes.statusuploadfilecsv}/> : "")
                    }
                  </Grid>
                </Container>
              </div>
            </fieldset>
            </Grid>
          </Grid>
        </Container>

      </CardContent>
      <Divider />
      <Box
        display="flex"
        justifyContent="flex-end"
        p={2}
      >
      </Box>
    </Card>
  );
};

FormSearchDevice.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default FormSearchDevice;