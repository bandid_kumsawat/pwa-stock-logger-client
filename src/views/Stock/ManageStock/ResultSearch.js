  
import React from 'react';
import clsx from 'clsx'
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container,
  Grid,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Box,
  Button,
  Stepper,
  Step,
  StepLabel,
  LinearProgress,
  CircularProgress,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Link
} from '@material-ui/core';
import {
  Autocomplete
} from '@material-ui/lab';
import Page from 'src/components/Page';
import {
  useParams
} from "react-router-dom";
import server from './../../../apis'
import moment from 'moment'
import FiberManualRecordSharpIcon from '@material-ui/icons/FiberManualRecordSharp';
import CheckCircleSharpIcon from '@material-ui/icons/CheckCircleSharp';
import CommentSharpIcon from '@material-ui/icons/CommentSharp';

const object_month = {
  January: "มกราคม",
  February: "กุมภาพันธ์",
  March: "มีนาคม",
  April: "เมษายน",
  May: "พฤษภาคม",
  June: "มิถุนายน",
  July: "กรกฎาคม",
  August: "สิงหาคม",
  September: "กันยายน",
  October: "ตุลาคม",
  November: "พฤศจิกายน",
  December: "ธันวาคม", 
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    width: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
  cardroot:{},
  title: {
    // fontSize: "24px",
    marginBottom: "6px",
    fontWeight: "600"
  },
  value:{
    // fontSize: "18px",
    fontWeight: "100"
  },
  CardHeaderStyle: {
    fontSize: "30px !important" 
  },
  ContainerDetail: {
    display: 'flex', 
    justifyContent: "center"
  },
  ItemDetail:{
    flexDirection: "row"
  },
  ItemArea: {
    marginLeft: "5px"
  },
  ItemBranch: {
    marginLeft: "10px",
    color: "#2946e8"
  },
  ItemIcon: {
    marginLeft: "5px",
    marginTop: "3px"
  },
  ItemNote: {
    flexDirection: "row"
  },
  contentNote: {

  },
  IconNote: {
    fontSize: "12px",
    marginTop: "6px",
    marginRight: "4px"
  },
  ItemManage: {
    flexDirection: "row"
  },
  IconManage: {
    fontSize: "12px",
    marginTop: "6px",
    marginRight: "4px"
  },

}));


const ResultSearch = ({ className, ...rest}) => {
  const classes = useStyles();
  var { DEVICE } = useParams();

  const [activeStep, setActiveStep] = React.useState(0);
  const [findStatus, setfindStatus] = React.useState(false)
  const [logger, setlogger] = React.useState([])
  const [steps, setsteps] = React.useState([])
  const [putInstock, setputInstock] = React.useState(true)
  const [area, setarea] = React.useState([])
  const [branch, setbranch] = React.useState([])
  const [selectarea, setselectarea] = React.useState('')
  const [selectranch, setselectranch] = React.useState('')
  const [note, setnote] = React.useState('')
  const [opencomponentsDialogInstock, setopencomponentsDialogInstock] = React.useState(false)
  const [componentsDialogInstockAgain, setcomponentsDialogInstockAgain] = React.useState(false)
  const [componentsDialogMaintennent, setcomponentsDialogMaintennent] = React.useState(false)
  const [opencomponentsDialogInstall, setopencomponentsDialogInstall] = React.useState(false)
  const [opencomponentsDialogInstallAfterMaintennant, setopencomponentsDialogInstallAfterMaintennant] = React.useState(false)
  const [statusoutstock, setstatusoutstock] = React.useState(false)
  const [selectstatus, setselectstatus] = React.useState('')
  const [putInstall, setputInstall] = React.useState(true)
  const [open, setOpen] = React.useState(false);

  const handleClickOpenDialog = (status) => {
    setOpen(true);
    setselectranch('')
    setselectstatus(status)
  };

  const handleClose = () => {
    setOpen(false);
    setopencomponentsDialogInstock (false)
    setcomponentsDialogInstockAgain(false)
    setcomponentsDialogMaintennent(false)
  };

  const handleUpdateStatus = async (e) => {
    if ((selectarea !== '' && selectranch !== '') || selectarea === 'กปภ.สำนักงานใหญ่'){
      setstatusoutstock(true)
      // setOpen(false);
      var req = {}
      if (selectstatus === 'Instock'){
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_instock": moment(new Date()).format("L h:mm:ss a"),
          "note": note,
        }
      }else if (selectstatus === 'Takeout') {
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_takeout": moment(new Date()).format("L h:mm:ss a"),
          "note": note,
        }
      }else if (selectstatus === 'Installed') {
        req = {
          // "serial": DEVICE,
          // "area": selectarea,
          // "branch": selectranch,
          // "status": selectstatus,
          "serial": DEVICE,
          "area": logger[0].area,
          "branch": logger[0].branch,
          "status": selectstatus,
          "time_installed": moment(new Date()).format("L h:mm:ss a"),
          "note": note,
        }
      }else if (selectstatus === 'Maintenance'){
        req = {
          "serial": DEVICE,
          "area": selectarea,
          "branch": selectranch,
          "status": selectstatus,
          "time_maintenance": moment(new Date()).format("L h:mm:ss a"),
          "note": note,
        }
      }
      console.log(req)
      const res = await server().post('/logger/update/id', req)
      if (res.status === 200){
        console.log(res)
        // window.location.replace('/#/app/Logger/' + DEVICE)
        window.location.reload()
        
      }
    }
  }
  const handleComfirmAgainDeviceMaintennant = async () => {
    setputInstock(false)
    var req = {
      "serial": DEVICE,
      "area": selectarea,
      "branch": selectranch,
      "status": "Maintenance",
      "time_maintenance": moment(new Date()).format("L h:mm:ss a"),
      "note": note,
    }
    const res = await server().post('/logger/update/id', req)
    if (res.status === 200){
      console.log(res)
      // window.location.replace('/#/app/Logger/' + DEVICE)
      window.location.reload()
    }
  }
  const handleComfirmAgainDeviceInStock = async () => {
    setputInstock(false)
    var req = {
      "serial": DEVICE,
      "area": selectarea,
      "branch": selectranch,
      "status": "Instock",
      "time_instock": moment(new Date()).format("L h:mm:ss a"),
      "note": note,
    }
    const res = await server().post('/logger/update/id', req)
    if (res.status === 200){
      console.log(res)
      // window.location.replace('/#/app/Logger/' + DEVICE)
      window.location.reload()
    }
  }
  const handleComfirmInstalledDeviceInStock = async () => {
    setputInstall(false)
    // handleClickOpenDialog("Installed")
    var req = {
      "serial": DEVICE,
      "area": logger[0].area,
      "branch": logger[0].branch,
      "status": 'Installed',
      "time_installed": moment(new Date()).format("L h:mm:ss a"),
      "note": note,
    }
    const res = await server().post('/logger/update/id', req)
    if (res.status === 200){
      console.log(res)
      // window.location.replace('/#/app/Logger/' + DEVICE)
      window.location.reload()
    }
  }
  const handleComfirmInstallAfterMaintennantDeviceInStock = async () => {
    // handleClickOpenDialog("Installed")
    setputInstall(false)
    var req = {
      "serial": DEVICE,
      "area": selectarea,
      "branch": selectranch,
      "status": 'Installed',
      "time_installed": moment(new Date()).format("L h:mm:ss a"),
      "note": note,
    }
    console.log(req)
    const res = await server().post('/logger/update/id', req)
    if (res.status === 200){
      console.log(res)
      // window.location.replace('/#/app/Logger/' + DEVICE)
      window.location.reload()
    }
  }
  const componentsDialogInstalledAfterMaintennant = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={opencomponentsDialogInstallAfterMaintennant}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">{"เลือกเขตและสาขาที่จะนำล๊อกเกอร์ไปติดตั้ง"}</DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          ><Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <Autocomplete
                hidden={!putInstall}
                id="combo-box-area"
                options={area}
                getOptionLabel={(option) => option.area}
                style={{ width: '100%' }}
                onChange={(event, data) => {
                  if (data !== null){
                    setselectarea(data.area)
                    setbranch(data.branch.map((item) => {
                      return {
                        branch: item.ww_desc
                      }
                    }))
                  }
                }}
                renderInput={(params) => <TextField {...params} label="เลือกเขต" variant="outlined" />}
              />
              <br></br>
              <Autocomplete
                hidden={branch.length === 0 || branch.length === 1 || !putInstall}
                id="combo-box-branch"
                options={branch}
                clearOnEscape={true}
                getOptionLabel={(option) => option.branch}
                style={{ width: '100%' }}
                onChange={(e, d) => {
                  if (d !== null){
                    console.log(d.branch)
                    setselectranch(d.branch)
                  }
                }}
                renderInput={(params) => <TextField {...params} label="เลือกสาขา" variant="outlined" />}
              />
              <div hidden={putInstall}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstall}><CircularProgress  /></div>
              <br></br>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หมายเหตุ"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button hidden={!putInstall} onClick={handleComfirmInstallAfterMaintennantDeviceInStock} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const componentsDialogInstalled = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={opencomponentsDialogInstall}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"></DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          ><Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              {
                ((logger.length !== 0) ? <div hidden={!putInstall}>{"ยืนยันการติดตั้งล๊อกเกอร์ " +DEVICE+ " ที่ " + logger[0].area +" "+ logger[0].branch}</div> : "")
              }
              <div hidden={putInstall}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstall}><CircularProgress  /></div>
              <br></br>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หมายเหตุ"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button hidden={!putInstall} onClick={handleComfirmInstalledDeviceInStock} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const componentsDialogMaintennentDialog = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={componentsDialogMaintennent}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"></DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          ><Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <div hidden={!putInstock}>{"ต้องการนำ "+DEVICE+" ซ่อมบำรุงที่ไหน"}</div>
              <div hidden={putInstock}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstock}><CircularProgress  /></div>
              <div hidden={!putInstock}>
                <Autocomplete
                  id="combo-box-area"
                  options={area}
                  getOptionLabel={(option) => option.area}
                  style={{ width: '100%' }}
                  onChange={(event, data) => {
                    if (data !== null){
                      setselectarea(data.area)
                      setbranch(data.branch.map((item) => {
                        return {
                          branch: item.ww_desc
                        }
                      }))
                    }
                  }}
                  renderInput={(params) => <TextField {...params} label="เลือกเขต" variant="outlined" style={{marginTop: "20px"}} />}
                />
                <br></br>
                <Autocomplete
                  hidden={branch.length === 0 || branch.length === 1}
                  id="combo-box-branch"
                  options={branch}
                  clearOnEscape={true}
                  getOptionLabel={(option) => option.branch}
                  style={{ width: '100%' }}
                  onChange={(e, d) => {
                    if (d !== null){
                      console.log(d.branch)
                      setselectranch(d.branch)
                    }
                  }}
                  renderInput={(params) => <TextField {...params} label="เลือกสาขา" variant="outlined" />}
                /><br></br>
              </div>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หัวข้อที่เสีย"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button hidden={!putInstock} onClick={handleComfirmAgainDeviceMaintennant} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const componentsDialogInstockAgainDialog = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={componentsDialogInstockAgain}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"></DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          ><Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <div hidden={!putInstock}>{"ต้องการนำ "+DEVICE+" เข้าสต๊อกที่ไหน"}</div>
              <div hidden={putInstock}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstock}><CircularProgress  /></div>
              <div hidden={!putInstock}>
                <Autocomplete
                  id="combo-box-area"
                  options={area}
                  getOptionLabel={(option) => option.area}
                  style={{ width: '100%' }}
                  onChange={(event, data) => {
                    if (data !== null){
                      setselectarea(data.area)
                      setbranch(data.branch.map((item) => {
                        return {
                          branch: item.ww_desc
                        }
                      }))
                    }
                  }}
                  renderInput={(params) => <TextField {...params} label="เลือกเขต" variant="outlined" />}
                />
                <br></br>
                <Autocomplete
                  hidden={branch.length === 0 || branch.length === 1}
                  id="combo-box-branch"
                  options={branch}
                  clearOnEscape={true}
                  getOptionLabel={(option) => option.branch}
                  style={{ width: '100%' }}
                  onChange={(e, d) => {
                    if (d !== null){
                      console.log(d.branch)
                      setselectranch(d.branch)
                    }
                  }}
                  renderInput={(params) => <TextField {...params} label="เลือกสาขา" variant="outlined" />}
                /><br></br>
              </div>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หมายเหตุ"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button hidden={!putInstock} onClick={handleComfirmAgainDeviceInStock} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const componentsDialogInstock = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={opencomponentsDialogInstock}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title"></DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          ><Grid
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <div hidden={!putInstock}>{"คุณต้องการนำล๊อกเกอร์ " + DEVICE + " เข้าสต๊อกไหม"}</div><br></br>
              <div hidden={putInstock}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstock}><CircularProgress  /></div>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หมายเหตุ"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button hidden={!putInstock} onClick={handleComfirmInsertDeviceInStock} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const componentsDialog = () => {
    return <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      fullWidth={true}
      maxWidth={'sm'}
      open={open}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">{"เลือกเขตและสาขาที่จะนำล๊อกเกอร์ไปติดตั้ง"}</DialogTitle>
      <DialogContent>
        <Container>
          <Grid
            container
            spacing={3}
          >
            <Grid
              hidden={!statusoutstock}
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            > <CircularProgress /> </Grid>
            <Grid
              hidden={statusoutstock}
              item
              lg={12}
              md={12}
              xl={12}
              xs={12}
            >
              <Autocomplete
                hidden={!putInstall}
                id="combo-box-area"
                options={area}
                getOptionLabel={(option) => option.area}
                style={{ width: '100%' }}
                onChange={(event, data) => {
                  if (data !== null){
                    setselectarea(data.area)
                    setbranch(data.branch.map((item) => {
                      return {
                        branch: item.ww_desc
                      }
                    }))
                  }
                }}
                renderInput={(params) => <TextField {...params} label="เลือกเขต" variant="outlined" />}
              />
              <br></br>
              <Autocomplete
                hidden={branch.length === 0 || branch.length === 1 || !putInstall}
                id="combo-box-branch"
                options={branch}
                clearOnEscape={true}
                getOptionLabel={(option) => option.branch}
                style={{ width: '100%' }}
                onChange={(e, d) => {
                  if (d !== null){
                    console.log(d.branch)
                    setselectranch(d.branch)
                  }
                }}
                renderInput={(params) => <TextField {...params} label="เลือกสาขา" variant="outlined" />}
              />
              <div hidden={putInstall}>กรุณารอซักครู่. . .</div>
              <div hidden={putInstall}><CircularProgress  /></div>
              <br></br>
              <TextField
                hidden={!putInstock}
                id="outlined-multiline-static"
                label="หมายเหตุ"
                multiline
                style={{width: "100%"}}
                rows={6}
                defaultValue=""
                variant="outlined"
                onChange={(e) => {
                  setnote(e.target.value)
                }}
              />
            </Grid>
          </Grid>
        </Container>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleUpdateStatus} color="primary">
          ยืนยัน
        </Button>
        <Button autoFocus onClick={handleClose} color="primary">
          ยกเลิก
        </Button>
      </DialogActions>
    </Dialog>
  }

  const handleComfirmInsertDeviceInStock = async () => {
    setputInstock(false)
    const res = await server().put("/logger/add", {
      "serial": DEVICE,
      "area": "กปภ.สำนักงานใหญ่",
      "branch": "",
      "status": "Instock",
      "time_instock": moment(new Date()).format("L h:mm:ss a"),
      "time_takeout": "",
      "time_installed": "",
      "note": note
    })
    if (res.data.status && typeof res.data.status !== undefined){
      // window.location.replace('/#/app/Logger/' + DEVICE)
      window.location.reload()
    }
  }

  React.useEffect(() => {
    const instock = () => {
      return <>
        <div>เข้าสต๊อก</div>
        <div
          hidden={!putInstock}
        >
          <Button  
            style={{marginTop: "10px"}} 
            variant="contained" 
            color="primary"
            onClick={ async () => {
              // setputInstock(false)
              setopencomponentsDialogInstock(true)
              // const res = await server().put("/logger/add", {
              //   "serial": DEVICE,
              //   "area": "กปภ.สำนักงานใหญ่",
              //   "branch": "",
              //   "status": "Instock",
              //   "time_instock": moment(new Date()).format("L h:mm:ss a"),
              //   "time_takeout": "",
              //   "time_installed": ""
              // })
              // if (res.data.status && typeof res.data.status !== undefined){
              //   window.location.replace('/#/app/Logger/' + DEVICE)
              // }
            }}
          >
            ยืนยัน
          </Button>
        </div>
        <div hidden={putInstock}><CircularProgress  /></div>
      </>
    }
    const outstock = () => {
      return <>
        <div>ออกจากสต๊อก</div>
        <Button  
          style={{marginTop: "10px"}} 
          variant="contained" 
          color="primary"
          onClick={async () => {
            handleClickOpenDialog("Takeout")
          }}
        >
          ดำเนินการ
        </Button>
      </>
    }
    const InstallAfterMaintennant = (data) => {
      return <>
        <div>ติดตั้ง</div>
        <Button  
          style={{marginTop: "10px"}} 
          variant="contained" 
          color="primary"
          onClick={async () => {
            setopencomponentsDialogInstallAfterMaintennant(true)
          }}
        >
          ดำเนินการ
        </Button>
      </>
    }
    const Installed = (data) => {
      return <>
        <div>ติดตั้ง</div>
        <Button  
          style={{marginTop: "10px"}} 
          variant="contained" 
          color="primary"
          onClick={async () => {
            setopencomponentsDialogInstall(true)
            // // handleClickOpenDialog("Installed")
            // var req = {
            //   "serial": DEVICE,
            //   "area": data.area,
            //   "branch": data.branch,
            //   "status": 'Installed',
            //   "time_installed": moment(new Date()).format("L h:mm:ss a"),
            //   "note": '',
            // }
            // const res = await server().post('/logger/update/id', req)
            // console.log(res)
            // window.location.replace('/#/app/Logger/' + DEVICE)
          }}
        >
          ดำเนินการ
        </Button>
      </>
    }
    const findDevice = async () => {
      const resProvice = await server().get('/logger/stock/provice')
      setarea(resProvice.data.map((item) => {
        return {
          area: item.reg_desc,
          branch: item.ww_all
        }
      }))
      const res = await server().post('/logger/find/id', {
        "serial": DEVICE
      })
      if (res.status === 200){
        console.log(res.data)
        res.data = res.data.map((row, index) => {
          var cvalue = row.time_instock
          var month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_instock =  ((row.time_instock === "") ? "" : moment(row.time_instock, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_takeout
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_takeout =  ((row.time_takeout === "") ? "" : moment(row.time_takeout, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_installed
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_installed =  ((row.time_installed === "") ? "" : moment(row.time_installed, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          
          cvalue = row.time_maintenance
          month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
          row.time_maintenance =  ((row.time_maintenance === "") ? "" : moment(row.time_maintenance, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
          return row
        })
        setlogger(res.data)
        setfindStatus(true)
        if (res.data.length === 0){
          setActiveStep(0)
          setsteps([
            instock(), 
            <>
              <div>ออกจากสต๊อก</div>
            </>, 
            'ติดตั้ง'
          ])
        }else{
          console.log(res.data[0])
          if (res.data[0].status === 'Instock'){
            setActiveStep(1)
            setsteps([
              <>
                <div>เข้าสต๊อก</div>
                <div>{
                  // moment(res.data[0].time_instock, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')
                  res.data[0].time_instock
                }</div>
              </>, 
              
              outstock() , 
              
              'ติดตั้ง'
            ])
          }else if (res.data[0].status === 'Takeout'){
            setActiveStep(2)
            setsteps([
              <>
                <div>เข้าสต๊อก</div>
                <div>{ res.data[0].time_instock /*moment(res.data[0].time_instock, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')*/}</div>
              </>, 
              <>
                <div>ออกจากสต๊อก</div>
                <div>{ res.data[0].time_takeout /*moment(res.data[0].time_takeout, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')*/}</div>
              </> , 
              
              Installed(res.data[0])
            ])
          }else if (res.data[0].status === 'Installed'){
            setActiveStep(3)
            setsteps([
              <>
                <div>เข้าสต๊อก</div>
                <div>{
                  //moment(res.data[0].time_instock, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')
                  res.data[0].time_instock
                }</div>
              </>, 
              <>
                <div>ออกจากสต๊อก</div>
                <div>{
                  //moment(res.data[0].time_takeout, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')
                  res.data[0].time_takeout
                }</div>
              </> , 
              <>
                <div>ติดตั้งแล้ว</div>
                <div>{
                  //moment(res.data[0].time_installed, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')
                  res.data[0].time_installed
                }</div>
              </>
            ])
          }else if (res.data[0].status === 'Maintenance'){
            setActiveStep(1)
            setsteps([
              <>
                <div>กำลังซ่อมบำรุง</div>
                <div>{
                  //moment(res.data[0].time_instock, "L LTS").add(543, 'year').format('DD MMMM YYYY HH:mm:ss')
                  res.data[0].time_instock
                }</div>
              </>,
              InstallAfterMaintennant(res.data[0])
            ])
          }
        }
      }
    }
    
    findDevice()  
  }, [DEVICE, putInstock])

  const contentHeader = () => {
    if (logger.length === 0){
      return (
        <div className={classes.ContainerDetail}>
          <div className={classes.ItemDetail + " " + classes.ItemIcon}>
            <FiberManualRecordSharpIcon style = {{color: "#3f51b5", fontSize: '14px'}}/>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemArea}>
            <h4>ล๊อกเกอร์<u>ยังไม่เข้าสต๊อก</u></h4>
          </div>
        </div>
      )
    }else if (logger[0].status === "Instock"){
      return (
        <div className={classes.ContainerDetail}>
          <div className={classes.ItemDetail + " " + classes.ItemIcon}>
            <FiberManualRecordSharpIcon style = {{color: "#3f51b5", fontSize: '14px'}}/>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemArea}>
            <h4>ล๊อกเกอร์<u>จัดเก็บในสต๊อก</u>ที่</h4>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemBranch}>
            <h4>{logger[0].area + " " + logger[0].branch}</h4>
          </div>
        </div>
      )
    }else if (logger[0].status === "Takeout"){
      return (
        <div className={classes.ContainerDetail}>
          <div className={classes.ItemDetail + " " + classes.ItemIcon}>
            <FiberManualRecordSharpIcon style = {{color: "#3f51b5", fontSize: '14px'}}/>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemArea}>
            <h4>ขณะนี้ล๊อกเกอร์<u>ออกจากสต๊อก</u>จะถูกนำไปติดตั้งที่</h4>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemBranch}>
            <h4>{logger[0].area + " " + logger[0].branch}</h4>
          </div>
        </div>
      )
    }else if (logger[0].status === "Installed"){
      return (
        <div className={classes.ContainerDetail}>
          <div className={classes.ItemDetail + " " + classes.ItemIcon}>
            <CheckCircleSharpIcon style = {{color: "#3f51b5", fontSize: '14px'}}/>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemArea}>
            <h4>ล๊อกเกอร์<u>ติดตั้งเสร็จเรียบร้อย</u>แล้วที่</h4>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemBranch}>
            <h4>{logger[0].area + " " + logger[0].branch}</h4>
          </div>
        </div>
      )
    }else if (logger[0].status === "Maintenance"){
      return (
        <div className={classes.ContainerDetail}>
          <div className={classes.ItemDetail + " " + classes.ItemIcon}>
            <CheckCircleSharpIcon style = {{color: "#3f51b5", fontSize: '14px'}}/>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemArea}>
            <h4>ล๊อกเกอร์<u>ซ่อมบำรุง</u>อยู่ที่</h4>
          </div>
          <div className={classes.ItemDetail + " " + classes.ItemBranch}>
            <h4>{logger[0].area + " " + logger[0].branch}</h4>
          </div>
        </div>
      )
    }
  }

  return (
    <Page
      className={classes.root}
      title="จัดการข้อมูล"
      {...rest}
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        ><Grid
            item
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            {
              componentsDialog()
            }
            {
              componentsDialogInstalled()
            }
            {
              componentsDialogInstock()
            }
            {
              componentsDialogInstockAgainDialog()
            }
            {
              componentsDialogMaintennentDialog()
            }
            {
              componentsDialogInstalledAfterMaintennant()
            }
            <div hidden={findStatus}>กำลังค้นหา. . . <LinearProgress /></div>
            <Card
              hidden={!findStatus}
              className={clsx(classes.cardroot, className)}
            >
              <CardHeader
                titleTypographyProps={{ variant:'h3' }}
                title={((logger.length === 0) ? "Serial Number: " + DEVICE : "Serial Number: " + DEVICE)}
              />
              <Divider />
              <CardContent>
                {
                  contentHeader()
                }
                <div className={classes.ContainerDetail}>
                  <div className={classes.ItemNote + " " + classes.IconNote } hidden = {logger.length === 0}>
                    <CommentSharpIcon style={{fontSize:"14px"}}/>
                  </div>
                  <div className={classes.ItemNote + " " + classes.contentNote} hidden = {logger.length === 0}>
                    หมายเหตุ: {((logger.length === 0) ? "" : ((logger[0].note === '') ? "-" : logger[0].note))}
                  </div>
                </div>

                <div className={classes.ContainerDetail}>
                  <div className={classes.ItemManage + " " + classes.contentNote} hidden={((logger.length !== 0) ? ((logger[0].status === "Installed") ? false : true) : true)}>
                    [
                      <Link
                      component="button"
                      variant="body2"
                      onClick={() => {
                        setcomponentsDialogInstockAgain(true)
                      }}
                    >
                      นำเข้าสต๊อก
                    </Link>, 
                    <Link
                      component="button"
                      variant="body2"
                      onClick={() => {
                        setcomponentsDialogMaintennent(true)
                      }}
                    >
                      ซ่อมบำรุง
                    </Link>]
                    
                  </div>
                </div>

                <Stepper alternativeLabel activeStep={activeStep}>
                  {steps.map((tagHTML, index) => (
                    <Step key={index}>
                      <StepLabel>{tagHTML}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </CardContent>
              <Divider />
              <Box
                display="flex"
                justifyContent="flex-end"
                p={2}
              >
                <Button 
                  variant="contained" 
                  color="primary" 
                  className={classes.btnsearch} 
                  onClick={
                    () => {
                      window.location.replace('/#/app/Manage')
                    }
                  }
                >
                  {'ออก'}
                </Button>
              </Box>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  )
};

ResultSearch.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default ResultSearch;