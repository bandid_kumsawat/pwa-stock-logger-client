
import React from 'react';
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container,
  Grid,
  LinearProgress
} from '@material-ui/core';
import Page from 'src/components/Page';
import FromSearchDevice from './FormSerachDevice'
import { store } from './../../../AppStore/store'
import { STORESTOCK, STOREPROVICE, EXPORT } from './../../../AppStore/actions'
import server from './../../../apis'
import moment from 'moment'

const object_month = {
  January: "มกราคม",
  February: "กุมภาพันธ์",
  March: "มีนาคม",
  April: "เมษายน",
  May: "พฤษภาคม",
  June: "มิถุนายน",
  July: "กรกฎาคม",
  August: "สิงหาคม",
  September: "กันยายน",
  October: "ตุลาคม",
  November: "พฤศจิกายน",
  December: "ธันวาคม", 
}

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
}));

const MangeStock = ({ className, ...rest}) => {
  const classes = useStyles();

  const [data, setdata] = React.useState([])

  React.useEffect(() => {
    async function callapi() {
      if (store.getState().Stock.length === 0){
        const res = await server().get('/logger/stock/history')
        var temp = res.data
        store.dispatch(EXPORT(temp))
        if (res.status === 200){
          res.data = res.data.map((row, index) => {
            var cvalue = row.time_instock
            var month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
            row.time_instock =  ((row.time_instock === "") ? "" : moment(row.time_instock, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
            
            cvalue = row.time_takeout
            month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
            row.time_takeout =  ((row.time_takeout === "") ? "" : moment(row.time_takeout, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
            
            cvalue = row.time_installed
            month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
            row.time_installed =  ((row.time_installed === "") ? "" : moment(row.time_installed, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
            
            cvalue = row.time_maintenance
            month = object_month[moment(cvalue, "L LTS").add(543, 'year').format('MMMM')]
            row.time_maintenance =  ((row.time_maintenance === "") ? "" : moment(row.time_maintenance, "L LTS").add(543, 'year').format('DD '+month+' YYYY HH:mm:ss'))
            return row
          })
        }
        store.dispatch(STORESTOCK(res.data))
        setdata(store.getState().Stock)
        const resProvice = await server().get('/logger/stock/provice')
        store.dispatch(STOREPROVICE(resProvice.data))
      }else{
        const resProvice = await server().get('/logger/stock/provice')
        store.dispatch(STOREPROVICE(resProvice.data))
        setdata(store.getState().Stock)
      }
    }
    callapi()
  }, [])

  return (
    <Page
      className={classes.root}
      title="ค้นหาล๊อกเกอร์"
      {...rest}
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        ><Grid
            item
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            {
              ((data.length === 0) ? <><div>กำลังค้นหา . . .</div><LinearProgress  className={classes.statusuploadfilecsv}/></> : <FromSearchDevice store={store}/>)
            }
          </Grid>
        </Grid>
      </Container>
    </Page>
  )
};

MangeStock.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default MangeStock;