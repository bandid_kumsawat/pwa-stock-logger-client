
import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  makeStyles,
  Container,
} from '@material-ui/core';
import QrReader from 'react-qr-reader'

const useStyles = makeStyles(() => ({
  root: {},
}));


const QrScanCode = ({ className, store,  ...rest}) => {
  const classes = useStyles();

  // const [state, setState] = React.useState({
  //   result: "No Result"
  // });

  const handleScan = (data) => {
    if (data) {
      window.location.replace('/#/app/Logger/'+data)
      // setState({ result: data });
    }
  }
  const handleError = (err) => {
    console.error(err)
  }

  React.useEffect(() => {

  }, [store])

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
      <CardHeader
        title="สแกน Qr Code"
      />
      <Divider />
      <CardContent>
        
        <Container maxWidth={false}>
          <QrReader
            delay={300}
            resolution={1000}
            legacyMode={false}
            // showViewFinder={false}
            onError={handleError}
            onScan={handleScan}
            style={{ width: '100%' }}
          />
          {/* <p>{state.result}</p> */}
        </Container>

      </CardContent>
      <Divider />
    </Card>
  );
};

QrScanCode.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default QrScanCode;