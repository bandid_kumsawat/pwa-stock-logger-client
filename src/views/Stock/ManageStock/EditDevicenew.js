
import React from 'react';
import clsx from 'clsx'
import PropTypes from 'prop-types';
import {
  makeStyles,
  Container,
  Grid,
  Card,
  CardHeader,
  Divider,
  CardContent,
  Box,
  Button,
  Stepper,
  Step,
  StepLabel
} from '@material-ui/core';
import {
} from '@material-ui/lab';
import Page from 'src/components/Page';
import {
  useParams
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    width: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3),
  },
  cardroot:{},
  title: {
    // fontSize: "24px",
    marginBottom: "6px",
    fontWeight: "600"
  },
  value:{
    // fontSize: "18px",
    fontWeight: "100"
  },
  CardHeaderStyle: {
    fontSize: "30px !important" 
  }
}));



const EditDevice = ({ className, ...rest}) => {
  const classes = useStyles();
  var { DEVICE } = useParams();
  console.log(DEVICE)
  const [activeStep, setActiveStep] = React.useState(1);

  const getSteps = () => {
    return [<><div>เข้าสต๊อก</div><div>11/22/2563 11:43:22 น.</div></>, <><div>ออกจากสต๊อก</div><Button  style={{marginTop: "10px"}} variant="contained" color="primary">ดำเนินการ</Button></>, 'ติดตั้ง'];
  }
  const steps = getSteps();

  return (
    <Page
      className={classes.root}
      title="จัดการข้อมูล"
      {...rest}
    >
      <Container maxWidth={false}>
        <Grid
          container
          spacing={3}
        ><Grid
            item
            lg={12}
            md={12}
            xl={12}
            xs={12}
          >
            <Card
              className={clsx(classes.cardroot, className)}
            >
              <CardHeader
                // title={((ListParam.length === 0) ? "จัดการข้อมูลล๊อกเกอร์": "แก้ไขข้อมูลล๊อกเกอร์")}
                className={classes.CardHeaderStyle}
                title="ค้นหาล๊อกเกอร์"
              />
              <Divider />
              <CardContent>
                <Stepper alternativeLabel activeStep={activeStep}>
                  {steps.map((label) => (
                    <Step key={label}>
                      <StepLabel>{label}</StepLabel>
                    </Step>
                  ))}
                </Stepper>
              </CardContent>
              <Divider />
              <Box
                display="flex"
                justifyContent="flex-end"
                p={2}
              >
                <Button 
                  variant="contained" 
                  color="primary" 
                  className={classes.btnsearch} 
                  onClick={
                    () => {
                      window.location.replace('/#/app/Manage')
                    }
                  }
                >
                  {'ออก'}
                </Button>
              </Box>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  )
};

EditDevice.propTypes = {
  className: PropTypes.string,
  store: PropTypes.object
};

export default EditDevice;