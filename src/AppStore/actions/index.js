export const STORESTOCK = (payload) => {
  return {
    type : "STORESTOCK",
    payload
  }
}

export const STOREPROVICE = (payload) => {
  return {
    type: "STOREPROVICE",
    payload
  }
}

export const RESULTSEARCHSTOCK = (payload) => {
  return {
    type: "RESULTSEARCHSTOCK",
    payload
  }
}

export const EXPORT = (payload) => {
  return {
    type: "EXPORT",
    payload
  }
}