import initialState from './state'

export default (state = initialState, action) => {
  switch (action.type) {
    case 'STORESTOCK':
      return Object.assign({}, state, {
        Stock: action.payload
      })
    case 'STOREPROVICE':
      return Object.assign({}, state, {
        Provice: action.payload
      })
    case 'RESULTSEARCHSTOCK':
      return Object.assign({}, state, {
        ResultSearchStock: action.payload
      })
    case 'EXPORT':
      return Object.assign({}, state, {
        Export: action.payload
      })
    default:
      return state
  }
}
