import axios from 'axios'

export default (token) => {
  var api = axios.create({
    baseURL: "http://127.0.0.1:4000/api"
    // baseURL: "https://dwdev.info/api"
    // http://104.155.176.189/api
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}